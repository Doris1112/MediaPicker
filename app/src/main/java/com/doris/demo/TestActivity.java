package com.doris.demo;

import com.doris.media.picker.base.BaseActivity;
import com.doris.media.picker.utils.MediaUtils;
import com.doris.mediapicker.R;
import com.doris.mediapicker.databinding.ActivityTestBinding;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;

public class TestActivity extends BaseActivity<ActivityTestBinding> {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_test;
    }

    @Override
    protected void init() {
        QMUIStatusBarHelper.translucent(this);
        QMUIStatusBarHelper.setStatusBarLightMode(this);

        mBinding.topBar.setTitle("JavaTest");
        mBinding.topBar.addLeftBackImageButton().setOnClickListener((v) -> finish());

        mBinding.qrbFirstImage.setOnClickListener((v) ->
                MediaUtils.firstImageJava(this, mediaModel -> {
                    mBinding.tvFirstImage.setText(mediaModel.getPath());
                    return null;
                }));
    }
}
