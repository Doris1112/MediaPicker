package com.doris.demo

import android.graphics.Color
import android.view.View
import android.widget.AdapterView
import com.doris.media.picker.base.BaseActivity
import com.doris.media.picker.model.MediaPickerParameter
import com.doris.media.picker.result.contract.MediaPickerContract
import com.doris.media.picker.utils.debounceClick
import com.doris.mediapicker.R
import com.doris.mediapicker.databinding.ActivityMainBinding
import com.qmuiteam.qmui.util.QMUIStatusBarHelper
import org.jetbrains.anko.startActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val mParameter = MediaPickerParameter()

    override fun getLayoutId() = R.layout.activity_main

    override fun init() {
        QMUIStatusBarHelper.translucent(this)
        QMUIStatusBarHelper.setStatusBarLightMode(this)

        val turnMediaPicker = registerForActivityResult(MediaPickerContract()) {
            println("turnMediaPicker: ${it.requestCode}")
            if (it.isPicker) {
                println("turnMediaPicker: ${it.size()}")
                println("turnMediaPicker: ${it.getFirstPath()}")
                println("turnMediaPicker: ${it.getFirst().name}")
                mParameter.pickerData(it.getData())
            }
        }
        mBinding.topBar.setTitle(R.string.app_name)
        mBinding.topBar.addLeftTextButton("JavaTest", R.id.mediaPicker_topBar_java_test)
            .debounceClick {
                startActivity<TestActivity>()
            }
        mBinding.topBar.addRightTextButton("确定", R.id.mediaPicker_topBar_sure)
            .debounceClick {
                mParameter.backText(mBinding.etBack.text.toString())
                    .title(mBinding.etTitle.text.toString())
                    .sureText(mBinding.etSure.text.toString())
                    .pageSize(mBinding.etPageSize.text.toString().ifEmpty { "0" }.toInt())
                    .isLight(mBinding.cbLight.isChecked)
                    .showNum(mBinding.cbShowNum.isChecked)
                    .needPreview(mBinding.cbNeedPreview.isChecked)
                    .folder(mBinding.etFolder.text.toString())
                var numS = mBinding.etMin.text.toString()
                var min = if (numS.isEmpty()) 1 else numS.toInt()
                if (min < 1) min = 1
                mParameter.min(min)
                numS = mBinding.etMax.text.toString()
                var max = if (numS.isEmpty()) 1 else numS.toInt()
                if (max < 1) max = 1
                mParameter.max(max)
                numS = mBinding.etMinSize.text.toString()
                min = if (numS.isEmpty()) 1024 else numS.toInt()
                if (min < 0) min = 1024
                mParameter.minSize(min)
                numS = mBinding.etMaxSize.text.toString()
                max = if (numS.isEmpty()) 1 else numS.toInt()
                if (max < min) max = -1
                mParameter.maxSize(max)
                turnMediaPicker.launch(mParameter)
            }
        // 状态栏
        mBinding.rgStatus.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_status_light -> mParameter.statusThemeLight()
                R.id.rb_status_dark -> mParameter.statusThemeDark()
            }
        }
        // 返回按钮
        mBinding.rgBack.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_back -> mParameter.backIcon(R.mipmap.media_picker_back)
                R.id.rb_back1 -> mParameter.backIcon(R.mipmap.media_picker_back1)
                R.id.rb_back2 -> mParameter.backIcon(R.mipmap.media_picker_back2)
            }
        }
        mBinding.rgBackTextColorRes.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_backTextColorRes ->
                    mParameter.backTextColorRes(R.color.media_picker_topbar_text_btn_color)
                R.id.rb_backTextColorRes1 ->
                    mParameter.backTextColorRes(R.color.media_picker_sure_color1)
                R.id.rb_backTextColorRes2 ->
                    mParameter.backTextColorRes(R.color.media_picker_sure_color2)
            }
        }
        // 标题
        mBinding.rgTitleColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_titleColor -> mParameter.titleColorRes(R.color.topBarTitle)
                R.id.rb_titleColor1 -> mParameter.titleColorRes(R.color.red)
                R.id.rb_titleColor2 -> mParameter.titleColorRes(R.color.blue)
            }
        }
        // 确定按钮
        mBinding.rgSure.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_sure -> mParameter.sureIcon(0)
                R.id.rb_sure1 -> mParameter.sureIcon(R.mipmap.media_picker_sure1)
                R.id.rb_sure2 -> mParameter.sureIcon(R.mipmap.media_picker_sure2)
            }
        }
        mBinding.rgSureTextColorRes.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_sureTextColorRes ->
                    mParameter.sureTextColorRes(R.color.media_picker_topbar_text_btn_color)
                R.id.rb_sureTextColorRes1 ->
                    mParameter.sureTextColorRes(R.color.media_picker_sure_color1)
                R.id.rb_sureTextColorRes2 ->
                    mParameter.sureTextColorRes(R.color.media_picker_sure_color2)
            }
        }
        // 标题背景颜色、图片
        mBinding.rgTopBarColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_topBarColor -> mParameter.topBarColorRes(R.color.topBar)
                R.id.rb_topBarColor1 -> mParameter.topBarColorRes(R.color.transparent)
                R.id.rb_topBarColor2 -> mParameter.topBarColorRes(R.color.gray)
            }
        }
        mBinding.rgTopBarRes.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_topBarRes -> mParameter.topBarRes(0)
                R.id.rb_topBarRes1 -> mParameter.topBarRes(R.mipmap.media_picker_topbar1)
                R.id.rb_topBarRes2 -> mParameter.topBarRes(R.mipmap.media_picker_topbar2)
            }
        }
        // 页面背景颜色、图片
        mBinding.rgPageColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_pageColor -> mParameter.pageColor(Color.WHITE)
                R.id.rb_pageColor1 -> mParameter.pageColor(Color.BLACK)
                R.id.rb_pageColor2 -> mParameter.pageColor(Color.RED)
            }
        }
        mBinding.rgPageRes.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_pageRes -> mParameter.pageRes(0)
                R.id.rb_pageRes1 -> mParameter.pageRes(R.mipmap.media_picker_page_bg1)
                R.id.rb_pageRes2 -> mParameter.pageRes(R.mipmap.media_picker_page_bg2)
            }
        }
        // 加载控件颜色
        mBinding.rgProgressColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_progressColor -> mParameter.progressColorRes(R.color.colorAccent)
                R.id.rb_progressColor1 -> mParameter.progressColorRes(R.color.blue)
                R.id.rb_progressColor2 -> mParameter.progressColorRes(R.color.red)
            }
        }
        // 提示信息颜色
        mBinding.rgTipColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_tipColor -> mParameter.tipColor(Color.parseColor("#666666"))
                R.id.rb_tipColor1 -> mParameter.tipColor(Color.BLUE)
                R.id.rb_tipColor2 -> mParameter.tipColor(Color.RED)
            }
        }
        // 授权按钮文字颜色
        mBinding.rgPermissionBtnColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_permissionBtnColor -> mParameter.permissionBtnColorRes(R.color.colorAccent)
                R.id.rb_permissionBtnColor1 -> mParameter.permissionBtnColorRes(R.color.blue)
                R.id.rb_permissionBtnColor2 -> mParameter.permissionBtnColorRes(R.color.red)
            }
        }
        // 授权按钮边框颜色
        mBinding.rgPermissionBtnBorderColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_permissionBtnBorderColor ->
                    mParameter.permissionBtnBorderColorRes(R.color.colorAccent)
                R.id.rb_permissionBtnBorderColor1 ->
                    mParameter.permissionBtnBorderColorRes(R.color.blue)
                R.id.rb_permissionBtnBorderColor2 ->
                    mParameter.permissionBtnBorderColorRes(R.color.red)
            }
        }
        // 媒体图标颜色
        mBinding.rgMediaIconColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_mediaIconColor -> mParameter.mediaIconColorRes(R.color.colorPrimary)
                R.id.rb_mediaIconColor1 -> mParameter.mediaIconColorRes(R.color.blue)
                R.id.rb_mediaIconColor2 -> mParameter.mediaIconColorRes(R.color.red)
            }
        }
        // 一行几列
        mBinding.spinnerSpanCount.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) = Unit
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                mParameter.spanCount(position + 1)
            }
        }
        mBinding.spinnerSpanCount.setSelection(3)
        // 不可预览并显示选中数量时背景边框颜色
        mBinding.rgNumBorderColor.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_numBorderColor -> mParameter.numBorderColorRes(R.color.colorPrimary)
                R.id.rb_numBorderColor1 -> mParameter.numBorderColorRes(R.color.red)
                R.id.rb_numBorderColor2 -> mParameter.numBorderColorRes(R.color.blue)
            }
        }
        // 选择类型
        mBinding.rgType.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_type1 -> mParameter.image()
                R.id.rb_type2 -> mParameter.video()
                R.id.rb_type3 -> mParameter.audio()
            }
        }
    }
}