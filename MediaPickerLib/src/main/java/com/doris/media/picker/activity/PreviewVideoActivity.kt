package com.doris.media.picker.activity

import android.content.Context
import android.widget.ImageView
import cn.jzvd.Jzvd
import com.bumptech.glide.Glide
import com.doris.media.picker.R
import com.doris.media.picker.base.BaseActivity
import com.doris.media.picker.databinding.MediaPickerPreviewVideoActivityBinding
import com.doris.media.picker.model.MediaPickerConfig
import com.doris.media.picker.utils.debounceClick
import com.qmuiteam.qmui.util.QMUIStatusBarHelper
import org.jetbrains.anko.startActivity

class PreviewVideoActivity : BaseActivity<MediaPickerPreviewVideoActivityBinding>() {

    companion object {
        @JvmStatic
        fun show(context: Context?, path: String) {
            show(context, "", path)
        }

        @JvmStatic
        fun show(context: Context?, title: String, path: String) {
            context?.startActivity<PreviewVideoActivity>(
                MediaPickerConfig.MEDIA_PICKER_PREVIEW_TITLE to title,
                MediaPickerConfig.MEDIA_PICKER_PREVIEW_PATH to path
            )
        }
    }

    override fun getLayoutId() = R.layout.media_picker_preview_video_activity

    override fun init() {
        val path = intent.getStringExtra(MediaPickerConfig.MEDIA_PICKER_PREVIEW_PATH)
        if (path.isNullOrEmpty()) {
            finish()
            return
        }
        val title = intent.getStringExtra(MediaPickerConfig.MEDIA_PICKER_PREVIEW_TITLE) ?: ""
        // 状态栏文字白色
        QMUIStatusBarHelper.setStatusBarDarkMode(this)
        // 返回按钮
        mBinding.mediaPickerBack.debounceClick { finish() }
        // 视频
        mBinding.mediaPickerVideo.posterImageView.scaleType = ImageView.ScaleType.FIT_CENTER
        Glide.with(this).load(path)
            .into(mBinding.mediaPickerVideo.posterImageView)
        mBinding.mediaPickerVideo.setUp(path, title)
        mBinding.mediaPickerVideo.startVideoAfterPreloading()
    }

    override fun onBackPressed() {
        if (Jzvd.backPress()) return
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        Jzvd.releaseAllVideos()
    }
}