package com.doris.media.picker.activity

import android.content.Context
import com.bumptech.glide.Glide
import com.doris.media.picker.R
import com.doris.media.picker.base.BaseActivity
import com.doris.media.picker.databinding.MediaPickerPreviewImageActivityBinding
import com.doris.media.picker.model.MediaPickerConfig
import com.doris.media.picker.utils.debounceClick
import com.qmuiteam.qmui.util.QMUIStatusBarHelper
import org.jetbrains.anko.startActivity

class PreviewImageActivity : BaseActivity<MediaPickerPreviewImageActivityBinding>() {

    companion object {
        @JvmStatic
        fun show(context: Context?, path: String) {
            context?.startActivity<PreviewImageActivity>(
                MediaPickerConfig.MEDIA_PICKER_PREVIEW_PATH to path
            )
        }
    }

    override fun setContentViewBefore() {
        super.setContentViewBefore()
        overridePendingTransition(
            R.anim.media_picker_preview_scale_enter,
            R.anim.media_picker_preview_alpha_nothing
        )
    }

    override fun getLayoutId() = R.layout.media_picker_preview_image_activity

    override fun init() {
        val path = intent.getStringExtra(MediaPickerConfig.MEDIA_PICKER_PREVIEW_PATH)
        if (path.isNullOrEmpty()) {
            finish()
            return
        }
        // 状态栏文字白色
        QMUIStatusBarHelper.translucent(this)
        QMUIStatusBarHelper.setStatusBarDarkMode(this)
        // 图片
        Glide.with(this).load(path).into(mBinding.mediaPickerPhotoView)
        mBinding.mediaPickerPhotoView.debounceClick { finish() }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(
            R.anim.media_picker_preview_alpha_nothing,
            R.anim.media_picker_preview_scale_exit
        )
    }

}