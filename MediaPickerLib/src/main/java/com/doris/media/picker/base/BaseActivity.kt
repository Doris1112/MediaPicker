package com.doris.media.picker.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity() {

    protected abstract fun getLayoutId(): Int
    protected abstract fun init()

    protected lateinit var mBinding: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewBefore()
        mBinding = DataBindingUtil.setContentView(this, getLayoutId())
        init()
    }

    protected open fun setContentViewBefore() {

    }

}