package com.doris.media.picker.result

import com.doris.media.picker.model.MediaModel

class MediaPickerResult(val requestCode: Int) {

    private val resultData = ArrayList<MediaModel>()
    var isPicker = false
        internal set

    fun addData(data: ArrayList<MediaModel>) {
        isPicker = true
        resultData.clear()
        resultData.addAll(data)
    }

    fun size() = resultData.size

    fun getData() = resultData

    fun getFirst() = resultData[0]

    fun getPathData(): ArrayList<String> {
        val data = ArrayList<String>()
        resultData.forEach { data.add(it.path) }
        return data
    }

    fun getFirstPath() = resultData[0].path

}