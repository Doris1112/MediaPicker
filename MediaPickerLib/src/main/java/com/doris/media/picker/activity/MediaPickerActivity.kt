package com.doris.media.picker.activity

import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.doris.media.picker.R
import com.doris.media.picker.adapter.BaseMediaPickerAdapter
import com.doris.media.picker.adapter.MediaPickerAdapter
import com.doris.media.picker.adapter.MediaPickerListener
import com.doris.media.picker.adapter.MediaPickerRowAdapter
import com.doris.media.picker.base.BaseActivity
import com.doris.media.picker.databinding.MediaPickerActivityBinding
import com.doris.media.picker.model.MediaModel
import com.doris.media.picker.model.MediaPickerConfig
import com.doris.media.picker.model.MediaPickerParameter
import com.doris.media.picker.permission.PermissionInterceptor
import com.doris.media.picker.utils.MediaUtils
import com.doris.media.picker.utils.debounceClick
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import com.qmuiteam.qmui.util.QMUIStatusBarHelper
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import com.qmuiteam.qmui.widget.pullLayout.QMUIPullLayout
import com.qmuiteam.qmui.widget.pullLayout.QMUIPullLayout.PullAction

class MediaPickerActivity : BaseActivity<MediaPickerActivityBinding>(), MediaPickerListener {

    private var mType = "图片"
    private var mPageIndex = 0
    private var mPullAction: PullAction? = null
    private lateinit var mAdapter: BaseMediaPickerAdapter

    private var mTipDialog: QMUITipDialog? = null
    private var mMediaPlayer: MediaPlayer? = null
    private lateinit var mParameter: MediaPickerParameter

    override fun getLayoutId() = R.layout.media_picker_activity

    override fun init() {
        initParameter()
        initStatus()
        initTopBar()
        initPage()
        initList()
        if (hasPermission()) {
            initMedia()
        } else {
            mBinding.mediaPickerLoading.showNoPermission()
        }
    }

    private fun initParameter() {
        mParameter = intent.getParcelableExtra(MediaPickerConfig.MEDIA_PICKER)
            ?: MediaPickerParameter()
        val pickerData =
            intent.getParcelableArrayListExtra<MediaModel>(MediaPickerConfig.MEDIA_PICKER_DATA)
        if (!pickerData.isNullOrEmpty()) {
            mParameter.pickerData.clear()
            mParameter.pickerData.addAll(pickerData)
        }
        val pickerMimeType =
            intent.getStringArrayListExtra(MediaPickerConfig.MEDIA_PICKER_MIME_TYPE)
        if (!pickerMimeType.isNullOrEmpty()) {
            mParameter.mimeTyp.clear()
            mParameter.mimeTyp.addAll(pickerMimeType)
        }
    }

    private fun initStatus() {
        QMUIStatusBarHelper.translucent(this)
        if (mParameter.statusTheme == MediaPickerConfig.STATUS_THEME_DARK) {
            QMUIStatusBarHelper.setStatusBarDarkMode(this)
        } else {
            QMUIStatusBarHelper.setStatusBarLightMode(this)
        }
    }

    private fun initTopBar() {
        mType = when (mParameter.type) {
            MediaPickerConfig.TYPE_VIDEO -> "视频"
            MediaPickerConfig.TYPE_AUDIO -> "音频"
            else -> "图片"
        }
        // 返回按钮
        if (mParameter.backText.isEmpty()) {
            val backBtn = if (mParameter.backIcon == R.mipmap.media_picker_back)
                mBinding.mediaPickerTopBar.addLeftBackImageButton()
            else
                mBinding.mediaPickerTopBar.addLeftImageButton(
                    mParameter.backIcon, R.id.mediaPicker_topBar_back
                )
            backBtn.debounceClick { finish() }
        } else {
            val backBtn = mBinding.mediaPickerTopBar.addLeftTextButton(
                mParameter.backText, R.id.mediaPicker_topBar_back
            )
            if (mParameter.backTextColorRes != R.color.media_picker_topbar_text_btn_color)
                backBtn.setTextColor(
                    ContextCompat.getColorStateList(this, mParameter.backTextColorRes)
                )
            backBtn.debounceClick { finish() }
        }
        // 标题
        val titleView = if (mParameter.title.isEmpty()) {
            mBinding.mediaPickerTopBar.setTitle("选择$mType")
        } else {
            mBinding.mediaPickerTopBar.setTitle(mParameter.title)
        }
        val defaultTitleColor = ContextCompat.getColor(this, R.color.topBarTitle)
        val titleColor = ContextCompat.getColor(this, mParameter.titleColorRes)
        if (titleColor != defaultTitleColor) {
            titleView.setTextColor(titleColor)
        }
        // 确定按钮
        if (mParameter.sureIcon != 0) {
            val sureBtn = mBinding.mediaPickerTopBar
                .addRightImageButton(mParameter.sureIcon, R.id.mediaPicker_topBar_sure)
            sureBtn.scaleType = ImageView.ScaleType.CENTER_INSIDE
            sureBtn.debounceClick { sure() }
        } else {
            if (mParameter.sureText.isEmpty()) mParameter.sureText = "确定"
            val sureBtn = mBinding.mediaPickerTopBar
                .addRightTextButton(mParameter.sureText, R.id.mediaPicker_topBar_sure)
            if (mParameter.sureTextColorRes != R.color.media_picker_topbar_text_btn_color)
                sureBtn.setTextColor(
                    ContextCompat.getColorStateList(this, mParameter.sureTextColorRes)
                )
            sureBtn.debounceClick { sure() }
        }
        // 标题背景图片、颜色
        if (mParameter.topBarRes != 0) {
            mBinding.mediaPickerTopBar.setBackgroundResource(mParameter.topBarRes)
        } else if (mParameter.topBarColorRes != R.color.topBar) {
            mBinding.mediaPickerTopBar.setBackgroundColor(
                ContextCompat.getColor(this, mParameter.topBarColorRes)
            )
        }
    }

    private fun initPage() {
        if (mParameter.pageRes != 0) {
            mBinding.mediaPickerPager.setImageResource(mParameter.pageRes)
        } else {
            mBinding.mediaPickerPager.visibility = View.GONE
            if (mParameter.pageColor != Color.WHITE)
                mBinding.mediaPickerContainer.setBackgroundColor(mParameter.pageColor)
        }

        mBinding.mediaPickerLoading.setTipColor(mParameter.tipColor)
        if (mParameter.progressColorRes != R.color.colorAccent) {
            mBinding.mediaPickerLoading.setProgressColor(
                ContextCompat.getColor(this, mParameter.progressColorRes)
            )
        }
        if (mParameter.permissionBtnColorRes != R.color.colorAccent) {
            mBinding.mediaPickerLoading.setPermissionBtnColor(
                ContextCompat.getColor(this, mParameter.permissionBtnColorRes)
            )
        }
        if (mParameter.permissionBtnBorderColorRes != R.color.colorAccent) {
            mBinding.mediaPickerLoading.setPermissionBtnBorderColor(
                ContextCompat.getColor(this, mParameter.permissionBtnBorderColorRes)
            )
        }

        mBinding.mediaPickerLoading.setBtnClickListener {
            XXPermissions.with(this).permission(Permission.WRITE_EXTERNAL_STORAGE)
                .interceptor(PermissionInterceptor("存储权限说明", "用于访问手机媒体库，选择图片、视频、音频"))
                .request { _, allGranted ->
                    if (!allGranted) return@request

                    mBinding.mediaPickerLoading.showLoading()
                    initMedia()
                }
        }
    }

    private fun initList() {
        mBinding.mediaPickerPullLayout.setActionListener {
            mPullAction = it
            if (it.pullEdge == QMUIPullLayout.PULL_EDGE_BOTTOM) {
                mPageIndex++
                initMedia()
            }
        }

        if (mParameter.spanCount < 1) mParameter.spanCount = 1
        if (mParameter.spanCount > 5) mParameter.spanCount = 5

        mAdapter = if (mParameter.spanCount == 1) {
            mBinding.mediaPickerRecycler.layoutManager = object : LinearLayoutManager(this) {
                override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
                    return RecyclerView.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                }
            }
            MediaPickerRowAdapter(mParameter, this)
        } else {
            mBinding.mediaPickerRecycler.layoutManager =
                object : GridLayoutManager(this, mParameter.spanCount) {
                    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
                        return RecyclerView.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                    }
                }
            MediaPickerAdapter(mParameter, this)
        }

        (mBinding.mediaPickerRecycler.itemAnimator as SimpleItemAnimator)
            .supportsChangeAnimations = false
        mBinding.mediaPickerRecycler.adapter = mAdapter
    }

    private fun hasPermission(): Boolean {
        val hasPermission = XXPermissions.isGranted(this, Permission.MANAGE_EXTERNAL_STORAGE)
                || XXPermissions.isGranted(this, Permission.WRITE_EXTERNAL_STORAGE)
                || XXPermissions.isGranted(this, Permission.READ_EXTERNAL_STORAGE)
        val readMediaPermission = when (mParameter.type) {
            MediaPickerConfig.TYPE_VIDEO ->
                XXPermissions.isGranted(this, Permission.READ_MEDIA_VIDEO)
            MediaPickerConfig.TYPE_AUDIO ->
                XXPermissions.isGranted(this, Permission.READ_MEDIA_AUDIO)
            else ->
                XXPermissions.isGranted(this, Permission.READ_MEDIA_IMAGES)
        }
        return hasPermission || readMediaPermission
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == XXPermissions.REQUEST_CODE) {
            if (hasPermission()) {
                mBinding.mediaPickerLoading.showLoading()
                initMedia()
            }
        }
    }

    private fun initMedia() {
        when (mParameter.type) {
            MediaPickerConfig.TYPE_VIDEO -> loadVideo()
            MediaPickerConfig.TYPE_AUDIO -> loadAudio()
            else -> loadImage()
        }
    }

    private fun loadImage() {
        MediaUtils.loadImage(
            this, mParameter.mimeTyp, mParameter.minSize,
            mParameter.maxSize, mParameter.folder, mParameter.pageSize, mPageIndex
        ) { loadOver(it) }
    }

    private fun loadVideo() {
        MediaUtils.loadVideo(
            this, mParameter.mimeTyp, mParameter.minSize,
            mParameter.maxSize, mParameter.folder, mParameter.pageSize, mPageIndex
        ) { loadOver(it) }
    }

    private fun loadAudio() {
        MediaUtils.loadAudio(
            this, mParameter.mimeTyp, mParameter.minSize,
            mParameter.maxSize, mParameter.folder, mParameter.pageSize, mPageIndex
        ) { loadOver(it) }
    }

    private fun loadOver(it: ArrayList<MediaModel>) {
        if (it.isEmpty()) {
            if (mPageIndex == 0) mBinding.mediaPickerLoading.showTip("暂无$mType")
        } else {
            if (mPageIndex == 0) {
                mBinding.mediaPickerLoading.hide()
                mAdapter.submitList(it)
                mBinding.mediaPickerRecycler.scrollToPosition(0)
            } else {
                mAdapter.addAll(it)
            }
        }
        if (mPullAction != null) {
            mBinding.mediaPickerPullLayout.finishActionRun(mPullAction!!)
            mPullAction = null
        }
    }

    private fun sure() {
        if (mParameter.min > mAdapter.getPickerSize()) {
            showTip("最少${mParameter.min}${getUnit()}$mType")
            return
        }
        val pickerIntent = Intent()
        pickerIntent.putExtra(MediaPickerConfig.MEDIA_PICKER_DATA, mAdapter.getPickerModel())
        setResult(MediaPickerConfig.MEDIA_PICKER_RESULT_OK, pickerIntent)
        finish()
    }

    override fun onPreview(view: View, model: MediaModel) {
        when (model.type) {
            MediaPickerConfig.TYPE_IMAGE -> {
                PreviewImageActivity.show(this, model.path)
            }
            MediaPickerConfig.TYPE_VIDEO -> {
                PreviewVideoActivity.show(this, model.name, model.path)
            }
            MediaPickerConfig.TYPE_AUDIO -> {
                if (mMediaPlayer?.isPlaying == true &&
                    mAdapter.getPlayAudioPosition() == mAdapter.itemIndexOfFirst(model)
                ) {
                    stopPlayAudio()
                    mAdapter.updatePlayAudioPosition(-1)
                } else {
                    startPlayAudio(model)
                }
            }
        }
    }

    private fun startPlayAudio(model: MediaModel) {
        try {
            stopPlayAudio()
            mMediaPlayer = MediaPlayer()
            mMediaPlayer?.setDataSource(model.path)
            mMediaPlayer?.setOnPreparedListener { it.start() }
            mMediaPlayer?.setOnCompletionListener {
                mAdapter.updatePlayAudioPosition(-1)
            }
            mMediaPlayer?.isLooping = false
            mMediaPlayer?.prepare()
            mAdapter.updatePlayAudioPosition(mAdapter.itemIndexOfFirst(model))
        } catch (e: Exception) {
            e.printStackTrace()
            stopPlayAudio()
            mAdapter.updatePlayAudioPosition(-1)
        }
    }

    private fun stopPlayAudio() {
        mMediaPlayer?.stop()
        mMediaPlayer?.reset()
        mMediaPlayer?.release()
        mMediaPlayer = null
    }

    override fun showMaxTip() {
        showTip("最多${mParameter.max}${getUnit()}$mType")
    }

    private fun getUnit() = if (mParameter.type == MediaPickerConfig.TYPE_IMAGE) "张" else "个"

    private fun showTip(msg: String) {
        dismissTip()
        mTipDialog = QMUITipDialog.Builder(this)
            .setIconType(QMUITipDialog.Builder.ICON_TYPE_INFO)
            .setTipWord(msg).create()
        mTipDialog?.show()
        Handler(Looper.getMainLooper()).postDelayed({ dismissTip() }, 1000)
    }

    private fun dismissTip() {
        if (mTipDialog != null && mTipDialog?.isShowing == true) {
            mTipDialog?.dismiss()
            mTipDialog = null
        }
    }

    override fun onPause() {
        super.onPause()
        stopPlayAudio()
    }

    override fun onDestroy() {
        super.onDestroy()
        dismissTip()
    }
}