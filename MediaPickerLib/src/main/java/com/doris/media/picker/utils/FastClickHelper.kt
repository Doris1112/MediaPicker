package com.doris.media.picker.utils

import android.view.View
import android.widget.Checkable
import com.doris.media.picker.R

var <T : View> T.lastClickTime: Long
    set(value)  = setTag(R.id.last_click_time, value)
    get() = getTag(R.id.last_click_time) as? Long ?: 0

fun <T : View> T.debounceClick(time: Long = 150, onClickListener: View.OnClickListener) {
    setOnClickListener {
        val currentTimeMillis = System.currentTimeMillis()
        if (currentTimeMillis - lastClickTime > time || this is Checkable) {
            lastClickTime = currentTimeMillis
            onClickListener.onClick(this)
        }
    }
}

inline fun <T : View> T.debounceClick(time: Long = 150, crossinline block: (T) -> Unit) {
    setOnClickListener {
        val currentTimeMillis = System.currentTimeMillis()
        if (currentTimeMillis - lastClickTime > time || this is Checkable) {
            lastClickTime = currentTimeMillis
            block(this)
        }
    }
}