package com.doris.media.picker.app

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.qmuiteam.qmui.arch.QMUISwipeBackActivityManager

open class MediaPickerApp: Application() {

    override fun onCreate() {
        super.onCreate()
        QMUISwipeBackActivityManager.init(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}