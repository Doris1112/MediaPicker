package com.doris.media.picker.widget

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.doris.media.picker.databinding.MediaPickerLoadingViewBinding
import com.doris.media.picker.utils.debounceClick

class LoadingView : ConstraintLayout {

    private lateinit var mBinding: MediaPickerLoadingViewBinding

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        mBinding = MediaPickerLoadingViewBinding
            .inflate(LayoutInflater.from(context), this, true)
    }

    fun setProgressColor(color: Int) {
        mBinding.mediaPickerProgress.indeterminateTintList = ColorStateList.valueOf(color)
    }

    fun setTipColor(color: Int) {
        mBinding.mediaPickerTip.setTextColor(color)
    }

    fun setPermissionBtnColor(color: Int) {
        mBinding.mediaPickerBtn.setTextColor(color)
    }

    fun setPermissionBtnBorderColor(color: Int) {
        mBinding.mediaPickerBtn.setStrokeColors(ColorStateList.valueOf(color))
    }

    fun hide() {
        visibility = View.GONE
    }

    fun showLoading() {
        showLoading("正在加载")
    }

    fun showLoading(tip: String) {
        visibility = View.VISIBLE

        mBinding.mediaPickerTip.text = tip
        mBinding.mediaPickerTip.visibility = View.VISIBLE
        mBinding.mediaPickerProgress.visibility = View.VISIBLE

        mBinding.mediaPickerBtn.visibility = View.GONE
    }

    fun showTip(tip: String) {
        visibility = View.VISIBLE

        mBinding.mediaPickerTip.text = tip
        mBinding.mediaPickerTip.visibility = View.VISIBLE

        mBinding.mediaPickerProgress.visibility = View.GONE
        mBinding.mediaPickerBtn.visibility = View.GONE
    }

    fun showNoPermission() {
        showTipBtn("无法访问本地存储", "去授权")
    }

    fun showTipBtn(tip: String, btn: String) {
        visibility = View.VISIBLE

        mBinding.mediaPickerTip.text = tip
        mBinding.mediaPickerTip.visibility = View.VISIBLE

        mBinding.mediaPickerBtn.text = btn
        mBinding.mediaPickerBtn.visibility = View.VISIBLE

        mBinding.mediaPickerProgress.visibility = View.GONE
    }

    fun setBtnClickListener(listener: OnClickListener) {
        mBinding.mediaPickerBtn.debounceClick(150, listener)
    }
}