package com.doris.media.picker.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.*
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.chad.library.adapter4.BaseQuickAdapter
import com.chad.library.adapter4.viewholder.QuickViewHolder
import com.doris.media.picker.R
import com.doris.media.picker.model.MediaModel
import com.doris.media.picker.model.MediaPickerConfig
import com.doris.media.picker.model.MediaPickerParameter
import com.doris.media.picker.transformation.SizeTransformation
import com.doris.media.picker.utils.debounceClick
import com.qmuiteam.qmui.util.QMUIDisplayHelper
import com.qmuiteam.qmui.widget.QMUIRadiusImageView2
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseMediaPickerAdapter(
    protected val parameter: MediaPickerParameter,
    protected val listener: MediaPickerListener,
    private val layout: Int
) : BaseQuickAdapter<MediaModel, QuickViewHolder>() {

    protected val mPickerModels = ArrayList<MediaModel>()
    protected var mPlayAudioPosition = -1

    init {
        mPickerModels.addAll(parameter.pickerData)
    }

    fun initCover(holder: QuickViewHolder, item: MediaModel): QMUIRadiusImageView2 {
        val imageView2Cover = holder.getView<QMUIRadiusImageView2>(R.id.mediaPicker_cover)
        if (item.type == MediaPickerConfig.TYPE_AUDIO) {
            if (parameter.mediaIconColorRes != R.color.colorPrimary) {
                imageView2Cover.setColorFilter(
                    ContextCompat.getColor(context, parameter.mediaIconColorRes)
                )
            }
        } else {
            val thumb =
                if (item.type == MediaPickerConfig.TYPE_IMAGE) R.drawable.media_picker_icon_image
                else R.drawable.media_picker_icon_video
            val drawableThumb = ContextCompat.getDrawable(context, thumb)
            if (drawableThumb != null && parameter.mediaIconColorRes != R.color.colorPrimary) {
                drawableThumb.colorFilter = PorterDuffColorFilter(
                    ContextCompat.getColor(context, parameter.mediaIconColorRes),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
            val imageTag = imageView2Cover.tag
            val drawable = if (imageTag is Drawable) imageTag else drawableThumb
            Glide.with(context).load(item.path)
                .error(drawable).placeholder(drawable)
                .transform(SizeTransformation(500))
                .into(object : CustomTarget<Drawable>() {
                    override fun onLoadCleared(placeholder: Drawable?) = Unit
                    override fun onResourceReady(
                        resource: Drawable, transition: Transition<in Drawable>?
                    ) {
                        imageView2Cover.setImageDrawable(resource)
                        imageView2Cover.tag = resource
                    }
                })
        }
        return imageView2Cover
    }

    fun getPickerSize() = mPickerModels.size

    fun getPickerModel() = mPickerModels

    fun getPlayAudioPosition() = mPlayAudioPosition

    fun updatePlayAudioPosition(position: Int) {
        if (position == mPlayAudioPosition) return
        val oldPosition = mPlayAudioPosition
        mPlayAudioPosition = position
        notifyItemChanged(oldPosition)
        notifyItemChanged(mPlayAudioPosition)
    }

    override fun onCreateViewHolder(
        context: Context, parent: ViewGroup, viewType: Int
    ) = QuickViewHolder(layout, parent)
}

class MediaPickerAdapter(parameter: MediaPickerParameter, listener: MediaPickerListener) :
    BaseMediaPickerAdapter(parameter, listener, R.layout.media_picker_item) {

    override fun onBindViewHolder(holder: QuickViewHolder, position: Int, item: MediaModel?) {
        if (item == null) return

        val imageView2Cover = initCover(holder, item)
        if (item.type == MediaPickerConfig.TYPE_VIDEO || item.type == MediaPickerConfig.TYPE_AUDIO) {
            holder.setText(R.id.mediaPicker_duration, item.durationTransform)
        } else {
            holder.setText(R.id.mediaPicker_duration, "")
        }

        val checkBox = holder.getView<CheckBox>(R.id.mediaPicker_check)
        val checkBoxMask = holder.getView<View>(R.id.mediaPicker_check_mask)
        val textViewNum = holder.getView<TextView>(R.id.mediaPicker_num)
        val textViewNumOval = holder.getView<TextView>(R.id.mediaPicker_num_oval)
        val imageViewAudio = holder.getView<ImageView>(R.id.mediaPicker_audio_op)

        checkBox.visibility = View.GONE
        checkBoxMask.visibility = View.GONE
        checkBox.isChecked = mPickerModels.contains(item)

        textViewNum.visibility = View.GONE
        textViewNumOval.visibility = View.GONE

        val num = mPickerModels.indexOf(item)
        textViewNum.text = if (num < 0) "" else "${num + 1}"
        textViewNumOval.text = if (num < 0) "" else "${num + 1}"

        val numBg = textViewNum.background as GradientDrawable
        numBg.setStroke(
            QMUIDisplayHelper.dp2px(context, 1),
            ContextCompat.getColor(context, parameter.numBorderColorRes)
        )
        textViewNum.background = numBg

        if (parameter.needPreview) {
            imageView2Cover.debounceClick { listener.onPreview(imageView2Cover, item) }
            if (parameter.showNum) {
                textViewNumOval.visibility = View.VISIBLE
                textViewNumOval.debounceClick {
                    if (mPickerModels.contains(item)) {
                        val index = mPickerModels.indexOf(item)
                        mPickerModels.removeAt(index)
                        for (i in index until mPickerModels.size) {
                            notifyItemChanged(itemIndexOfFirst(mPickerModels[i]))
                        }
                        textViewNumOval.text = ""
                    } else {
                        if (mPickerModels.size < parameter.max) {
                            mPickerModels.add(item)
                            textViewNumOval.text = "${mPickerModels.size}"
                        } else {
                            if (parameter.max == 1) {
                                val notifyPosition = itemIndexOfFirst(mPickerModels[0])
                                mPickerModels.clear()
                                mPickerModels.add(item)
                                notifyItemChanged(notifyPosition)
                                textViewNumOval.text = "${mPickerModels.size}"
                            } else {
                                listener.showMaxTip()
                            }
                        }
                    }
                }
            } else {
                checkBox.visibility = View.VISIBLE
                checkBoxMask.visibility = View.VISIBLE
                checkBoxMask.debounceClick {
                    if (mPickerModels.contains(item)) {
                        val index = mPickerModels.indexOf(item)
                        mPickerModels.removeAt(index)
                        for (i in index until mPickerModels.size) {
                            notifyItemChanged(itemIndexOfFirst(mPickerModels[i]))
                        }
                        checkBox.isChecked = false
                    } else {
                        if (mPickerModels.size < parameter.max) {
                            mPickerModels.add(item)
                            checkBox.isChecked = true
                        } else {
                            if (parameter.max == 1) {
                                val notifyPosition = itemIndexOfFirst(mPickerModels[0])
                                mPickerModels.clear()
                                mPickerModels.add(item)
                                notifyItemChanged(notifyPosition)
                                checkBox.isChecked = true
                            } else {
                                listener.showMaxTip()
                            }
                        }
                    }
                }
            }

            if (item.type == MediaPickerConfig.TYPE_AUDIO) {
                imageViewAudio.visibility = View.VISIBLE
                if (mPlayAudioPosition == position)
                    imageViewAudio.setImageResource(R.drawable.media_picker_pause)
                else
                    imageViewAudio.setImageResource(R.drawable.media_picker_play)
            } else {
                imageViewAudio.visibility = View.GONE
            }
        } else {
            if (parameter.showNum) {
                if (mPickerModels.contains(item)) {
                    textViewNum.visibility = View.VISIBLE
                } else {
                    textViewNum.visibility = View.GONE
                }
            } else {
                checkBox.visibility = View.VISIBLE
            }
            imageView2Cover.debounceClick {
                if (mPickerModels.contains(item)) {
                    val index = mPickerModels.indexOf(item)
                    mPickerModels.removeAt(index)
                    for (i in index until mPickerModels.size) {
                        notifyItemChanged(itemIndexOfFirst(mPickerModels[i]))
                    }
                    checkBox.isChecked = false
                    textViewNum.visibility = View.GONE
                } else {
                    if (mPickerModels.size < parameter.max) {
                        mPickerModels.add(item)
                        checkBox.isChecked = true
                        textViewNum.text = "${mPickerModels.size}"
                        if (parameter.showNum) textViewNum.visibility = View.VISIBLE
                    } else {
                        if (parameter.max == 1) {
                            val notifyPosition = itemIndexOfFirst(mPickerModels[0])
                            mPickerModels.clear()
                            mPickerModels.add(item)
                            notifyItemChanged(notifyPosition)
                            checkBox.isChecked = true
                            textViewNum.text = "${mPickerModels.size}"
                            if (parameter.showNum) textViewNum.visibility = View.VISIBLE
                        } else {
                            listener.showMaxTip()
                        }
                    }
                }
            }
            imageViewAudio.visibility = View.GONE
        }
    }
}

class MediaPickerRowAdapter(parameter: MediaPickerParameter, listener: MediaPickerListener) :
    BaseMediaPickerAdapter(parameter, listener, R.layout.media_picker_item_row) {
    private val sdf = SimpleDateFormat("yyyy.MM.dd", Locale.CHINA)

    override fun onBindViewHolder(holder: QuickViewHolder, position: Int, item: MediaModel?) {
        if (item == null) return
        val imageView2Cover = initCover(holder, item)
        holder.setText(R.id.mediaPicker_name, item.name)
        val info = StringBuffer()
        if (item.type == MediaPickerConfig.TYPE_VIDEO || item.type == MediaPickerConfig.TYPE_AUDIO) {
            info.append(item.durationTransform).append("\t\t")
        } else {
            info.append(item.width).append("x").append(item.height).append("\t\t")
        }
        info.append(sdf.format(Date(item.lastModifed))).append("\t\t").append(item.sizeTransform)
        holder.setText(R.id.mediaPicker_info, info.toString())

        val itemView = holder.getView<ConstraintLayout>(R.id.mediaPicker_item)
        val checkBox = holder.getView<CheckBox>(R.id.mediaPicker_check)
        val checkBoxMask = holder.getView<View>(R.id.mediaPicker_check_mask)
        val textViewNumOval = holder.getView<TextView>(R.id.mediaPicker_num_oval)
        val imageViewAudio = holder.getView<ImageView>(R.id.mediaPicker_audio_op)

        if (parameter.isLight) {
            holder.setTextColor(R.id.mediaPicker_name, Color.WHITE)
            checkBox.buttonTintList = ColorStateList.valueOf(Color.WHITE)
            textViewNumOval.setBackgroundResource(R.drawable.media_picker_bg_check_num_oval)
        }

        checkBox.visibility = View.GONE
        checkBoxMask.visibility = View.GONE
        checkBox.isChecked = mPickerModels.contains(item)

        textViewNumOval.visibility = View.GONE

        val num = mPickerModels.indexOf(item)
        textViewNumOval.text = if (num < 0) "" else "${num + 1}"

        if (item.type == MediaPickerConfig.TYPE_AUDIO) {
            imageViewAudio.visibility = View.VISIBLE
            if (mPlayAudioPosition == position)
                imageViewAudio.setImageResource(R.drawable.media_picker_pause)
            else
                imageViewAudio.setImageResource(R.drawable.media_picker_play)
        }

        if (parameter.needPreview) {
            itemView.debounceClick { listener.onPreview(imageView2Cover, item) }
            if (parameter.showNum) {
                textViewNumOval.visibility = View.VISIBLE
                textViewNumOval.debounceClick {
                    if (mPickerModels.contains(item)) {
                        val index = mPickerModels.indexOf(item)
                        mPickerModels.removeAt(index)
                        for (i in index until mPickerModels.size) {
                            notifyItemChanged(itemIndexOfFirst(mPickerModels[i]))
                        }
                        textViewNumOval.text = ""
                    } else {
                        if (mPickerModels.size < parameter.max) {
                            mPickerModels.add(item)
                            textViewNumOval.text = "${mPickerModels.size}"
                        } else {
                            if (parameter.max == 1) {
                                val notifyPosition = itemIndexOfFirst(mPickerModels[0])
                                mPickerModels.clear()
                                mPickerModels.add(item)
                                notifyItemChanged(notifyPosition)
                                textViewNumOval.text = "${mPickerModels.size}"
                            } else {
                                listener.showMaxTip()
                            }
                        }
                    }
                }
            } else {
                checkBox.visibility = View.VISIBLE
                checkBoxMask.visibility = View.VISIBLE
                checkBoxMask.debounceClick {
                    if (mPickerModels.contains(item)) {
                        val index = mPickerModels.indexOf(item)
                        mPickerModels.removeAt(index)
                        for (i in index until mPickerModels.size) {
                            notifyItemChanged(itemIndexOfFirst(mPickerModels[i]))
                        }
                        checkBox.isChecked = false
                    } else {
                        if (mPickerModels.size < parameter.max) {
                            mPickerModels.add(item)
                            checkBox.isChecked = true
                        } else {
                            if (parameter.max == 1) {
                                val notifyPosition = itemIndexOfFirst(mPickerModels[0])
                                mPickerModels.clear()
                                mPickerModels.add(item)
                                notifyItemChanged(notifyPosition)
                                checkBox.isChecked = true
                            } else {
                                listener.showMaxTip()
                            }
                        }
                    }
                }
            }
        } else {
            if (parameter.showNum) {
                textViewNumOval.visibility = View.VISIBLE
            } else {
                checkBox.visibility = View.VISIBLE
            }
            itemView.debounceClick {
                if (mPickerModels.contains(item)) {
                    val index = mPickerModels.indexOf(item)
                    mPickerModels.removeAt(index)
                    for (i in index until mPickerModels.size) {
                        notifyItemChanged(itemIndexOfFirst(mPickerModels[i]))
                    }
                    checkBox.isChecked = false
                    textViewNumOval.text = ""
                } else {
                    if (mPickerModels.size < parameter.max) {
                        mPickerModels.add(item)
                        checkBox.isChecked = true
                        textViewNumOval.text = "${mPickerModels.size}"
                    } else {
                        if (parameter.max == 1) {
                            val notifyPosition = itemIndexOfFirst(mPickerModels[0])
                            mPickerModels.clear()
                            mPickerModels.add(item)
                            notifyItemChanged(notifyPosition)
                            checkBox.isChecked = true
                            textViewNumOval.text = "${mPickerModels.size}"
                        } else {
                            listener.showMaxTip()
                        }
                    }
                }
            }
        }
    }
}

interface MediaPickerListener {
    fun onPreview(view: View, model: MediaModel)

    fun showMaxTip()
}