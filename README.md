[![](https://jitpack.io/v/com.gitee.Doris1112/MediaPicker.svg)](https://jitpack.io/#com.gitee.Doris1112/MediaPicker)

# MediaPicker

#### 介绍
图片、视频、音频选择

#### 导入
1. root build.gradle
```
    allprojects {
        repositories {
            ...
            maven { url 'https://jitpack.io' }
        }
    }
```

2. module build.gradle
```
    dependencies {
        implementation 'com.gitee.Doris1112:MediaPicker:1.1.0
    }
```

#### 配置
1. Application 需要继承MediaPickerApp

2. Base application theme ->  parent="MediaPickerTheme"

3. AndroidManifest.xml
```
    <application
        android:name=".XXX"
        ...
        android:theme="@style/XXX"
        tools:replace="android:theme">
```
如果您有自己的FileProvider，mete-data需要加上tools:replace="android:resource"
```
    <provider
        android:name="androidx.core.content.FileProvider"
        android:authorities="${applicationId}.fileprovider"
        android:exported="false"
        android:grantUriPermissions="true">
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/XXX"
                tools:replace="android:resource" />
    </provider>
```


#### 使用
step1
```
    val mediaPicker = registerForActivityResult(MediaPickerContract()) {
        // 必须是选择了才有数据，否则getFirst*()会异常
        if (it.isPicker) {
            when(it.requestCode){
                yourself code1 -> {
                    println("选择数量: ${it.size()}")
                    println("第一个媒体: ${it.getFirst()}")
                    println("第一个媒体路径: ${it.getFirstPath()}")
                    println("选择ModelList: ${it.getData()}")
                    println("选择PathList: ${it.getPathData()}")
                }
                yourself code2 -> {}
                else -> {}
            }
        }
    }
```

step2
```
    mediaPicker.launch(MediaPickerParameter())
```

#### MediaPickerParameter
```
    statusThemeLight() // 状态栏文字黑色
    statusThemeDark() // 状态栏文字白色

    backIcon(Int) // 返回按钮显示图片
    backText(String) // 返回按钮显示文字
    backTextColorRes(Int) // 返回文字按钮颜色 R.color.ColorStateList

    title(String) // 标题
    titleColorRes(Int) // 标题颜色  R.color.Color

    sureIcon(Int) // 确定按钮图片
    sureText(String) // 确定按钮文字
    sureTextColorRes(Int) // 确定文字按钮颜色  R.color.ColorStateList

    topBarColorRes(Int) // 标题栏背景颜色  R.color.Color
    topBarRes(Int) // 标题栏背景图片

    pageColor(Int) // 页面背景颜色 Color
    pageRes(Int) // 页面背景图片

    tipColor(Int) // 提示文字颜色 Color
    progressColorRes(Int) // 加载时Pogress的颜色 R.color.Color
    permissionBtnColorRes(Int) // 授权按钮文字颜色 R.color.Color
    permissionBtnBorderColorRes(Int) // 授权按钮边框颜色 R.color.Color

    spanCount(Int) // 一行N列，默认4列
    pageSize(Int) // 一页N条，默认spanCount*16列
    isLight(Boolean) // 当spanCount=1时才有效，文字颜色是否为亮色（白色）默认不是
    showNum(Boolean) // 是否显示选中数量，默认不显示
    needPreview(Boolean) // 是否需要预览，默认需要
    numBorderColorRes(Int) // 不可预览并显示选中数量时背景边框颜色 R.color.Color
    mediaIconColorRes(Int) // 媒体图片颜色 R.color.Color

    min(Int) // 最少选择数量，默认1
    max(Int) // 最大选择数量，默认1
    minSize(Int) // 最小大小（单位B），默认1024
    maxSize(Int) // 最大大小（单位B），默认-1（无限大）
    folder(String) // 指定文件夹
    image() // 选择图片，默认
    video() // 选择视频
    audio() // 选择音频

    imageAndGif() // 选择图片时包含Gif

    mimeTyp(ArrayList<String>) // 自定义自己需要的媒体类型（MediaMimeType有默认类型可供参考）
    pickerData(ArrayList<MediaModel>) // 已选中的数据

    requestCode(Int)
```

#### MediaUtils
```
    // 获取第一张图片
    firstImage(
        context: Context?,
        mimeType: ArrayList<String> = MediaMimeType.IMAGE_DEFAULT,
        minSize: Int = 1024,
        maxSize: Int = -1,
        folder: String = "",
        orderBy: String = "${MediaColumn.dateModified} desc",
        callback: (MediaModel) -> Unit
    )
    firstImageJava(context: Context?, callback: (MediaModel) -> Unit)

    // 加载图片
    loadImage(
        context: Context?,
        mimeType: ArrayList<String> = MediaMimeType.IMAGE_DEFAULT,
        minSize: Int = 1024,
        maxSize: Int = -1,
        folder: String = "",
        orderBy: String = "${MediaColumn.dateModified} desc",
        callback: (ArrayList<MediaModel>) -> Unit
    )
    loadImageJava(context: Context?, callback: (ArrayList<MediaModel>) -> Unit)

    // 分页加载图片
    loadImage(
        context: Context?,
        mimeType: ArrayList<String> = MediaMimeType.IMAGE_DEFAULT,
        minSize: Int = 1024,
        maxSize: Int = -1,
        folder: String = "",
        pageSize: Int = 24,
        pageIndex: Int = 0,
        orderBy: String = "${MediaColumn.dateModified} desc",
        callback: (ArrayList<MediaModel>) -> Unit
    )
    fun loadImageJava(
        context: Context?, pageSize: Int, pageIndex: Int,
        callback: (ArrayList<MediaModel>) -> Unit
    )

    // 加载视频
    fun loadVideo(
        context: Context?,
        mimeType: ArrayList<String> = MediaMimeType.VIDEO_DEFAULT,
        minSize: Int = 1024,
        maxSize: Int = -1,
        folder: String = "",
        orderBy: String = "${MediaColumn.dateModified} desc",
        callback: (ArrayList<MediaModel>) -> Unit
    )
    loadVideoJava(context: Context?, callback: (ArrayList<MediaModel>) -> Unit)

    // 分页加载视频
    fun loadVideo(
        context: Context?,
        mimeType: ArrayList<String> = MediaMimeType.VIDEO_DEFAULT,
        minSize: Int = 1024,
        maxSize: Int = -1,
        folder: String = "",
        pageSize: Int = 24,
        pageIndex: Int = 0,
        orderBy: String = "${MediaColumn.dateModified} desc",
        callback: (ArrayList<MediaModel>) -> Unit
    )
    loadVideoJava(
        context: Context?, pageSize: Int, pageIndex: Int,
        callback: (ArrayList<MediaModel>) -> Unit
    )

    // 加载音频
    loadAudio(
        context: Context?,
        mimeType: ArrayList<String> = MediaMimeType.AUDIO_DEFAULT,
        minSize: Int = 1024,
        maxSize: Int = -1,
        folder: String = "",
        orderBy: String = "${MediaColumn.dateModified} desc",
        callback: (ArrayList<MediaModel>) -> Unit
    )
    loadAudioJava(context: Context?, callback: (ArrayList<MediaModel>) -> Unit)

    // 分页加载音频
    loadAudio(
        context: Context?,
        mimeType: ArrayList<String> = MediaMimeType.AUDIO_DEFAULT,
        minSize: Int = 1024,
        maxSize: Int = -1,
        folder: String = "",
        pageSize: Int = 24,
        pageIndex: Int = 0,
        orderBy: String = "${MediaColumn.dateModified} desc",
        callback: (ArrayList<MediaModel>) -> Unit
    )
    loadAudioJava(
        context: Context?, pageSize: Int, pageIndex: Int,
        callback: (ArrayList<MediaModel>) -> Unit
    )

    // 查询类型
    loadMimeType(context: Context?, type: Int): HashSet<String>
    type：MediaPickerConfig.TYPE_IMAGE、MediaPickerConfig.TYPE_VIDEO、MediaPickerConfig.TYPE_AUDIO

    // 查询指定路径的媒体类型
    queryMimeType(context: Context?, type: Int, path: String): String

    // 转换大小为B\KB\MB\GB
    transformSize(size: Long): String

    // 获取图片宽高
    getImageSize(path: String): Size

    // 获取视频宽高
    getVideoSize(path: String): Size

    // 检测视频是否存在音频
    hasAudio(path: String): Boolean

    // 获取媒体时长
    getMediaDuration(path: String): Long

    // 转换时间为00:01\01:00:00
    transformTime(d: Long, needHour: Boolean = false): String
    transformTimeJava(d: Long): String

    // 删除图片
    deleteImage(context?, imagePath)

    // 删除视频
    deleteVideo(context?, videoPath)

    // 删除音频
    deleteAudio(context?, audioPath)

    // 修改图片名称
    updateImageName(context?, oldPath, newPath)

    // 修改视频名称
    updateVideoName(context?, oldPath, newPath)

    // 修改音频名称
    updateAudioName(context?, oldPath, newPath)

    // 获取图片Uri
    findImageUriByPath(context: Context?, mediaPath: String?)

    // 获取视频Uri
    findVideoUriByPath(context: Context?, mediaPath: String?)

    // 获取音频Uri
    findAudioUriByPath(context: Context?, mediaPath: String?)

    // 刷新系统媒体库
    refreshSystemMedia(context: Context?, mediaPath: String?)
```

#### 其他
1. UI：[QMUI Android 2.0.0-alpha10](https://qmuiteam.com/android)
2. 图片：[Glide 4.13.2](https://github.com/bumptech/glide)   [PhotoView 2.3.0](https://github.com/Baseflow/PhotoView)
3. 视频：[JZVD 7.7.0](https://github.com/Jzvd/JZVideo)
4. 权限：[XXPermissions 18.5](https://github.com/getActivity/XXPermissions)
5. 适配器：[BaseRecyclerViewAdapterHelper 3.0.4](https://github.com/CymChad/BaseRecyclerViewAdapterHelper)
